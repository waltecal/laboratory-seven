/***********************************
 * Names: Caleb Walters, Wayne Pitts
 *              Damon Schneider
 * Date: 10/09/2019
 * Class: EGR 226-904
 * Instructor: Trevor Ekin
 * Description: This code initializes
 * and writes functions commonly
 * used in LCD display functions
 **********************************/
void ButtonSetup(){     //Creates ButtonSetup funtion
    P4->SEL0 &= ~0xFF;
    P4->SEL1 &= ~0xFF;  //Sets all pins to GPIO
    P4->DIR |= 0xFF;    //Sets all pins as an Output
    P4->OUT &= ~0xFF;   //Sets all pins as Low
}

void Delay_MS(int delay){   //Creates Delay_MS function
    SysTick->CTRL =0;        //Disable SysTick During step
    SysTick->LOAD = (3000 * delay);     //Max reload value
    SysTick->VAL = 9;       //Any write to current clears it
    SysTick->CTRL = 5;      //Enable systic, 3MHz, No Interrupts

    while(!(SysTick->CTRL & BIT(16)));  //While pin is not an input, wait
}

void Delay_US(int delay){   //Creates Delay_US function

    SysTick->CTRL =0;        //Disable SysTick During step
    SysTick->LOAD = (3 * delay);     //Max reload value
    SysTick->VAL = 9;       //Any write to current clears it
    SysTick->CTRL = 5;      //Enable systic, 3MHz, No Interrupts

    while(!(SysTick->CTRL & BIT(16)));  //While pin is not an input, wait
}

void PulseE(){      //Creates PulseE function
    P4->OUT &= ~BIT4;   //Sets P4.4 to Low
    Delay_US(10);       //Calls Delay_US for 10 micro seconds
    P4->OUT |= BIT4;    //Sets P4.4 to High
    Delay_US(10);       //Calls Delay_US for 10 micro seconds
    P4->OUT &= ~BIT4;   //Sets P4.4 to Low
    Delay_US(10);       //Calls Delay_US for 10 micro seconds
}

void PushNibble(uint8_t send){      //Creates PushNibble function
    P4->OUT &= ~0x0F;       //Sets P4.1-4.4 to Low
    P4->OUT |= (send & 0x0F);   //Sets P4.1-4.4 to High and allows to be sent
    PulseE();   //Calls the PulseE function
}

void PushByte(uint8_t send){      //Creates PushByte function
    PushNibble(send>>4);    //Calls PushNibble and says to shift screen right 4
    PushNibble(send & 0x0F);   //Calls PushNibble and says to allow sending P4.1-4.4
}

void CommandLCD(uint8_t send){            //Creates CommandLCD function
    P4->OUT &= ~BIT5;   //Sets P4.5 to Low
    PushByte(send);     //Allows function to be sent
    P4->OUT |= BIT5;    //Sets P4.5 to High
}

void LCD_int(){         //Creates LCD_int function
    P4->OUT &= ~BIT5;   //Sets P4.5 to Low
    PushNibble(3);      //Blinks cursor
    Delay_MS(10);       //Calls Delay_MS for 10 ms

    PushNibble(3);     //Blinks cursor
    Delay_MS(1);       //Calls Delay_MS for 1 ms

    PushNibble(3);     //Blinking cursor
    Delay_MS(1);       //Calls Delay_MS for 1 ms

    PushNibble(0x02);    //Blink the cursor
    Delay_MS(100);       //Calls Delay_MS for 100 ms

    PushByte(0x28);      //Blink the cursor
    Delay_MS(100);       //Calls Delay_MS for 100 ms

    PushByte(0x06);      //Blink the cursor
    Delay_MS(100);       //Calls Delay_MS for 100 ms

    PushByte(0x01);      //Blink the cursor
    Delay_MS(100);       //Calls Delay_MS for 100 ms

    PushByte(0x0F);      //Blink the cursor
    Delay_MS(100);       //Calls Delay_MS for 100 ms

    P4->OUT |= BIT5;     //Sets P4.5 to High
}
