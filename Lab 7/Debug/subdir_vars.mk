################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../msp432p401r.cmd 

C_SRCS += \
../LCD\ P1.c \
../LCD\ P2.c \
../main.c \
../startup_msp432p401r_ccs.c \
../system_msp432p401r.c 

C_DEPS += \
./LCD\ P1.d \
./LCD\ P2.d \
./main.d \
./startup_msp432p401r_ccs.d \
./system_msp432p401r.d 

OBJS += \
./LCD\ P1.obj \
./LCD\ P2.obj \
./main.obj \
./startup_msp432p401r_ccs.obj \
./system_msp432p401r.obj 

OBJS__QUOTED += \
"LCD P1.obj" \
"LCD P2.obj" \
"main.obj" \
"startup_msp432p401r_ccs.obj" \
"system_msp432p401r.obj" 

C_DEPS__QUOTED += \
"LCD P1.d" \
"LCD P2.d" \
"main.d" \
"startup_msp432p401r_ccs.d" \
"system_msp432p401r.d" 

C_SRCS__QUOTED += \
"../LCD P1.c" \
"../LCD P2.c" \
"../main.c" \
"../startup_msp432p401r_ccs.c" \
"../system_msp432p401r.c" 


