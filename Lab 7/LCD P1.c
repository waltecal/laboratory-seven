/***********************************
 * Names: Caleb Walters, Wayne Pitts
 *              Damon Schneider
 * Date: 10/09/2019
 * Class: EGR 226-904
 * Instructor: Trevor Ekin
 * Description: This code prints out
 * the names of group members on the
 * LCD display
 **********************************/
#include "msp.h"
#include "LCD.h"
#include <stdio.h>      //Includes necessary libraries to run this program
#include <stdlib.h>
#include <string.h>


/**
 * main.c
 */
void main(void)
{
    char name[17], name2[17], name3[17], name4[17];     //Initializes character variables
    int i=0;            //Initializes integer variable and sets value to 0

    strcpy(name, "Wayne Pitts");        //Says what will be printed on line 1
    strcpy(name2, "Caleb Walters");     //Says what will be printed on line 2
    strcpy(name3, "EGR");               //Says what will be printed on line 3
    strcpy(name4, "226");               //Says what will be printed on line 4

    ButtonSetup();      //Calls ButtonSetup function from LCD.h
    LCD_int();          //Calls LCD_int function from LCD.h, blinks cursor

    CommandLCD(0x82);           //Calls CommandLCD function from LCD.h to place cursor at 0x82, or the first line
    for(i=0;i<strlen(name); i++){   //Says to run this for loop if there is space on the sides, so as to centralize the names
    PushByte(name[i]);      //Runs if there is room; pushes text for line 1 until for loop stops running
    }

    CommandLCD(0xC1);       //Calls CommandLCD function from LCD.h to place cursor at 0xC1, or the second line

    for(i=0;i<strlen(name2); i++){      //Says to run this for loop if there is space on the sides, so as to centralize the names
    PushByte(name2[i]);     //Runs if there is room; pushes text for line 2 until for loop stops running
    }

    CommandLCD(0x96);       //Calls CommandLCD function from LCD.h to place cursor at 0x96, or the third line

    for(i=0;i<strlen(name3); i++){      //Says to run this for loop if there is space on the sides, so as to centralize the names
    PushByte(name3[i]);         //Runs if there is room; pushes text for line 3 until for loop stops running
    }

    CommandLCD(0xD6);       //Calls CommandLCD function from LCD.h to place cursor at 0xD6, or the fourth line

    for(i=0;i<strlen(name4); i++){      //Says to run this for loop if there is space on the sides, so as to centralize the names
    PushByte(name4[i]);         //Runs if there is room; pushes text for line 4 until for loop stops running
    }

    while(1);       //Keeps the function running
}
