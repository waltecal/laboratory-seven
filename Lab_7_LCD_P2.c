/***********************************
 * Names: Caleb Walters, Wayne Pitts
 *              Damon Schneider
 * Date: 10/09/2019
 * Class: EGR 226-904
 * Instructor: Trevor Ekin
 * Description: This code prints
 * "LABORATORY OVER" and scrolls
 * along the line in and out of view
 **********************************/
#include "msp.h"
#include "LCD.h"
#include <stdio.h>          //Initializes libraries needed for this program
#include <stdlib.h>
#include <string.h>


/**
 * main.c
 */
void main(void)
{
    char name[17];      //Initializes character varaible
    int i, j;       //Initializes integer variables

    strcpy(name, "LABORATORY OVER ");   //Says what will be printed on line 1

    ButtonSetup();      //Calls ButtonSetup function from LCD.h
    LCD_int();          //Calls LCD_int function from LCD.h
    for(j=0;1;j++){

        CommandLCD(0x80);       //Calls the CommandLCD function from LCD.h to place cursor at 0x80

        for(i=0;i<strlen(name); i++){       //Runs for statement below if there is space on the sides
            if((i+j) < 16){         //Runs for statement below if the amount of free spaces left on the LCD is less than 16, or the whole row
                PushByte(name[i+j]);        //Pushes text off the screen
            }
            else{                           //Runs once above for statement is not true
                PushByte(name[i+j-16]);     //Pushes text back onto screen
            }
        }
        Delay_MS(1000);     //Calls Delay_MS function from LCD.h, and delays for 1 second
        if(j==15){      //If the value of j gets to 15, reset the value to 0
            j=0;
        }
    }
}
