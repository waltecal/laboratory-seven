/************************************
 * Names: Caleb Walters, Wayne Pitts
 *              Damon Schneider
 * Date: 10/16/2019
 * Instructor: Prof. Trevor Ekin
 * Class: EGR 226-904
 * Description: This code allows a
 * user to input numbers on a keypad
 * to control the speed of a DC motor
 ***********************************/
#include "msp.h"
#include "keypad.h"         //Includes the libraries needed
#include <stdio.h>
#include <stdlib.h>
float dutycycle = .5;   //Declares the float as a global variable because it is outside of main

void SetupMotor();  //Initializes the SetupMotor function
/**
 * main.c
 */
void main(void)
{
    int newcycle = -1;      //Declares int newcycle and assigns it a value of -1

    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer
    //P6.6 1kHz 50% duty cycle
    SetupMotor();   //Calls SetupMotor function
    setupKeypad();      //Calls setupKeypad function

    while(1){   //runs

        newcycle = getKeypress();   //sets newcycle variable to getKeypress function, so as to read the what is typed on the keypad

        if(newcycle != -1){         //if newcycle is not -1
            dutycycle = (newcycle)*.1;      //multiply newcycle by .1 and set it t dutycycle
            printf("Duty cycle is %f\n", dutycycle);    //Prints the duty cycle
            SetupMotor();   //Calls SetupMotor function
        }



    }
}

void SetupMotor(){
    P6->SEL0 |= BIT7; //giving TA2.4 control of P6.6
    P6->SEL1 &= ~BIT7;      //Sets P6.7 to GPIO
    P6->DIR |= BIT7;        //Sets P6.7 to output

    TIMER_A2->CTL = 0b000001010010100; //SMLK, divide by 2, count up, resets timer
    TIMER_A2->CCR[0] = 37500; //3000000 / 40 / 2 for the period
    TIMER_A2->CCTL[4] = 0xE0; //set/reset mode
    TIMER_A2->CCR[4] = 37500 * dutycycle; //50% duty cycle for TA2.3 which is P6.6

}
