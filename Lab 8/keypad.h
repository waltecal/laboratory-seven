/************************************
 * Names: Caleb Walters, Wayne Pitts
 *              Damon Schneider
 * Date: 10/02/2019
 * Instructor: Prof. Trevor Ekin
 * Class: EGR 226-904
 * Description: This code initializes
 * pins for the keypad 
 ***********************************/
void StandardPins(){    //Creates function that initializes pins
P3->SEL0 &= ~BIT3;
P3->SEL1 &= ~BIT3;      //Sets pin to GPIO
P3->DIR &= ~BIT3;       //Sets P3.3 to Input
P3->REN |= BIT3;        //Enables Resistor on P3.3
P3->OUT |= BIT3;        //Sets input to High 

P4->SEL0 &= ~BIT3;
P4->SEL1 &= ~BIT3;      //Sets pin to GPIO
P4->DIR &= ~BIT3;       //Sets P4.3 to Input
P4->REN |= BIT3;        //Enables Resistor on P4.3
P4->OUT |= BIT3;        //Sets input to High 

P4->SEL0 &= ~BIT1;      
P4->SEL1 &= ~BIT1;      //Sets pin to GPIO
P4->DIR &= ~BIT1;       //Sets P4.1 to Input
P4->REN |= BIT1;        //Enables Resistor on P4.3
P4->OUT |= BIT1;        //Sets input to High 

P4->SEL0 &= ~BIT6;
P4->SEL1 &= ~BIT6;      //Sets pin to GPIO 
P4->DIR &= ~BIT6;       //Sets P4.6 to Input
P4->REN |= BIT6;        //Enables Resistor on P4.6
P4->OUT |= BIT6;        //Sets input to High

P1->SEL0 &= ~BIT5;
P1->SEL1 &= ~BIT5;      //Sets pin to GPIO
P1->DIR &= ~BIT5;       //Sets P1.5 to Input
P1->REN |= BIT5;        //Enables Resistor on P4.6
P1->OUT |= BIT5;        //Sets input to High 

P6->SEL0 &= ~BIT5;
P6->SEL1 &= ~BIT5;      //Sets pin to GPIO
P6->DIR &= ~BIT5;       //Sets P6.5 to Input
P6->REN |= BIT5;        //Enables Resistor on P6.5
P6->OUT |= BIT5;        //Sets input to High 

P6->SEL0 &= ~BIT4;
P6->SEL1 &= ~BIT4;      //Sets pin to GPIO
P6->DIR &= ~BIT4;       //Sets P6.4 to Input 
P6->REN |= BIT4;        //Enables Resistor on P6.4 
P6->OUT |= BIT4;        //Sets input to High 
return;
}

void SysTick_DB(){

    SysTick->CTRL =0;        //Disable SysTick During step
    SysTick->LOAD = 300000;     //Max reload value
    SysTick->VAL = 9;       //Any write to current clears it
    SysTick->CTRL = 5;      //Enable systic, 3MHz, No Interrupts

    while(!(SysTick->CTRL & BIT(16)));      //Wait for Systick Timer
}

int KeyPad(){       //Keypad function is created
            P3->DIR |= BIT3;        //Sets P3.3 to Output
            P3->OUT &= ~BIT3;       //Sets P3.3 to Low
            if(!(P4->IN & BIT6)){       //If P4.6 is not an input, run below
                SysTick_DB();       //Calls the Systick Timer function
                while(!(P4->IN & BIT6));    //While P4.6 is not an input, return the integer 1
                return 1;
            }
            if(!(P6->IN & BIT5)){       //If P6.5 is not an input, run below
                SysTick_DB();       //Calls the Systick Timer function 
                while(!(P6->IN & BIT5));    //While P6.5 is not an input, return the integer 2
                return 2;
            }
            if(!(P6->IN & BIT4)){       //If P6.4 is not an input, run below
                SysTick_DB();       //Calls the Systick Timer function 
                while(!(P6->IN & BIT4));    //While P6.4 is not an input, return the integer 3
                return 3;
            }
            StandardPins();     //Calls the Pin initialization function

            P4->DIR |= BIT1;    //Sets P4.1 to Output
            P4->OUT &= ~BIT1;   //Sets P4.1 to Low
            if(!(P4->IN & BIT6)){       //If P4.6 is not an input, run below 
                SysTick_DB();       //Calls the Systick Timer function 
                while(!(P4->IN & BIT6));    //While P4.6 is not an input, return the integer 4
                return 4;
            }
            if(!(P6->IN & BIT5)){       //If P6.5 is not an input, run below
                SysTick_DB();       //Calls the Systick Timer function 
                while(!(P6->IN & BIT5));    //While P6.5 is not an input, return the integer 5
                return 5;
            }
            if(!(P6->IN & BIT4)){       //If P6.4 is not an input, run below
                SysTick_DB();       //Calls the Systick Timer function 
                while(!(P6->IN & BIT4));    //While P6.4 is not an input, return the integer 6
                return 6;
            }
            StandardPins();     //Calls the Pin initialization function 

            P4->DIR |= BIT3;    //Sets P4.3 to Output
            P4->OUT &= ~BIT3;   //Sets P4.3 to Low
            if(!(P4->IN & BIT6)){       //If P4.6 is not an input, run below
                SysTick_DB();       //Calls the Systick Timer function 
                while(!(P4->IN & BIT6));    //While P4.6 is not an input, return the integer 7
                return 7;
            }
            if(!(P6->IN & BIT5)){       //If P6.5 is not an input, run below
                SysTick_DB();       //Calls the Systick Timer function 
                while(!(P6->IN & BIT5));    //While P6.5 is not an input, return the integer 8
                return 8;
            }
            if(!(P6->IN & BIT4)){       //If P6.4 is not an input, run below
                SysTick_DB();       //Calls the Systick Timer function 
                while(!(P6->IN & BIT4));    //While P6.4 is not an input, return the integer 9 
                return 9;
            }
            StandardPins();     //Calls the Pin initialization function

            P1->DIR |= BIT5;    //Sets P1.5 to Output 
            P1->OUT &= ~BIT5;   //Sets P1.5 to Low
            if(!(P4->IN & BIT6)){       //If P4.6 is not an input, run below
                SysTick_DB();       //Calls the Systick Timer function 
                while(!(P4->IN & BIT6));    //While P4.6 is not an input, return the integer 11, which will be represented by an asterisk 
                return 11;
            }
            if(!(P6->IN & BIT5)){       //If P6.5 is not an input, run below
                SysTick_DB();       //Calls the Systick Timer function 
                while(!(P6->IN & BIT5));    //While P6.5 is not an input, return the integer 0
                return 0;
            }
            if(!(P6->IN & BIT4)){       //If P6.4 is not an input, run below 
                SysTick_DB();       //Calls the Systick Timer function 
                while(!(P6->IN & BIT4));    //While P6.4 is not an input, return the integer 12, which will be represented by a hashtag, or lb sign
                return 12;
            }
            StandardPins();     //Calls the Pin initialization function
            return -1;      //Returns a value of -1
}

