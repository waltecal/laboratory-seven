/************************************
 * Names: Caleb Walters, Wayne Pitts
 *              Damon Schneider
 * Date: 10/16/2019
 * Instructor: Prof. Trevor Ekin
 * Class: EGR 226-904
 * Description: This code allows a
 * user to change the speed of a DC
 * in the input window
 ***********************************/
#include "msp.h"        //Includes necessary library


/**
 * main.c
 */
void main(void)
{
    float dutycycle = .8;   //Sets float dutycycle to .8
	WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer

	P6->SEL0 &= ~BIT7;
	P6->SEL1 &= ~BIT7;      //Sets P6.7 as a GPIO
	P6->DIR |= BIT7;        //Sets P6.7 as Outout
	P6->OUT &= ~BIT7;       //Sets P6.7 to Low

while(1){       //runs

    SysTick->CTRL &= ~BIT0;     //Disables SysTick Timer
    SysTick->LOAD = 3000000/40 * dutycycle;     //Divides the period by 40 and multiplies it by the duty cycle
    SysTick->VAL = 2;       //Clear current value to 2
    SysTick->CTRL |= BIT0;      //Enables Systick Timer

    while(!(SysTick->CTRL & BIT(16))){   //While the SysTick is not an Input
        P6->OUT |= BIT7;        //Sets P6.7 to High
    }

    SysTick->CTRL &= ~BIT0;     //Disables SysTick Timer
    SysTick->LOAD = 3000000/40 * (1-dutycycle);     //Divides the period by 40 and multiples it by 1 minus the duty cycle
    SysTick->VAL = 2;       //Clear current value to 2
    SysTick->CTRL |= BIT0;      //Enables SysTick Timer

    while(!(SysTick->CTRL & BIT(16))){  //While the Systick is not an Input
        P6->OUT &= ~BIT7;   //Sets P6.7 to Low
    }
}

}
