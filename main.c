#include "msp.h"
#include <stdio.h>

/**
 * main.c
 */

volatile uint16_t firstVal,secondVal,period;
volatile int Hz;



void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;     // stop watchdog timer

    //10 Hz output on P2.2
    P2->SEL0 &= ~BIT2;
    P2->SEL1 &= ~BIT2;
    P2->DIR  |=  BIT2;

    //14 Hz output on P2.0
    P2->SEL0 &=  ~BIT0;
    P2->SEL1 &= ~BIT0;
    P2->DIR  |=  BIT0;


    //Capture input on P2.4
    P2->SEL0 |=  BIT4;
    P2->SEL1 &= ~BIT4;
    P2->DIR  &= ~BIT4;

    TIMER_A0->CTL = 0b1011100100; //SMCLK, Continuous mode, divide by 1
    TIMER_A0->CCTL[1] = 0b0100100100010000;  //Capture Rising Edge, Synchronous, Interrupt
    NVIC_EnableIRQ(TA0_N_IRQn);
    __enable_interrupts();

    while(1){
      switch (Hz){
      case 10:
          P2->OUT |= BIT2;
          P2->OUT &=~ BIT0;
          break;
      case 14:
          P2->OUT |= BIT0;
          P2->OUT &=~ BIT2;
          break;
      default:
          P2->OUT &=~ BIT2;
          P2->OUT &=~ BIT0;
          break;
      }


        __delay_cycles(1500000);
    }
}

void TA0_N_IRQHandler()
{
    if(TIMER_A0->CCTL[1] & BIT0)
    {
        firstVal = secondVal;
        secondVal = TIMER_A0->CCR[1];
        period = secondVal - firstVal;
        Hz = ((375000/period) + 1);
    }
    TIMER_A0->CCTL[1] &= ~(BIT1|BIT0);
}

