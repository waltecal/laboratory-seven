/************************************
 * Names: Caleb Walters, Wayne Pitts
 *              Damon Schneider
 * Date: 10/23/2019
 * Instructor: Prof. Trevor Ekin
 * Class: EGR 226-904
 * Description: This code uses LED
 * interrupts to print different
 * numbers on a 7 segment LED
 ***********************************/
#include "msp.h"    //Includes necessary libraries


int Number = 0;     //Makes global variable Number and sets it equal to 0

void main(void)
{
	WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer
	 P4->SEL0 &= ~0x7F;
	    P4->SEL1 &= ~0x7F;        //Sets bits 7,6,5,4,3,2,1 to GPIO on port 4
	    P4->DIR |= 0x7F;          //Sets bits 7,6,5,4,3,2,1 as output
	    P4->OUT &= ~0x7F;         //Makes bits 7,6,5,4,3,2,1 a low output

	    __enable_interrupt();
	    NVIC_EnableIRQ(PORT5_IRQn);     //Enables Interrupts on Port 5
	    NVIC_EnableIRQ(PORT2_IRQn);     //Enables interrupts on Port 2

	    P5->SEL0 &= ~BIT6;
	    P5->SEL1 &= ~BIT6;        //Sets P5.6 as GPIO
	    P5->DIR &= ~BIT6;          //Sets P5.6 as Input
	    P5->REN |= BIT6;          //Enables Pull-up on 5.6
	    P5->OUT |= BIT6;         //Makes 5.6 a low input
	    P5->IE |= BIT6;         //Allows Interrupt Enable on P5.6


	    P2->SEL0 &= ~(BIT4|BIT6);
	    P2->SEL1 &= ~(BIT4|BIT6);        //Sets P2.4 and P2.6 as GPIO
	    P2->DIR &= ~(BIT4|BIT6);          //Sets P2.4 and P2.6 as Input
	    P2->REN |= (BIT4|BIT6);          //Enables Pull-up on P2.4 and P2.6
	    P2->OUT |= (BIT4|BIT6);         //Makes P2.4 and P2.6 as High
	    P2->IE |= (BIT4|BIT6);          //Enable Interrupt on P2.4 and P2.6

	    while(1){       //Runs
	        switch (Number){    //When number is... Run below
	        case -1:
	            Number = 9;     //Sets Number to 9
	            break;
	        case 0:
	            P4->OUT &= ~0xFF;   //Sets all bits/segments to Input
	            P4->OUT |= 0b00111111; //Uses segments to create a 0
	            break;
	        case 1:
	            P4->OUT &= ~0xFF;   //Sets all bits/segments to Input
	            P4->OUT |= 0b00000110; //Uses segments to create a 1
	            break;
	        case 2:
	            P4->OUT &= ~0xFF;   //Sets all bits/segments to Input
	            P4->OUT |= 0b01011011; //Uses segments to create a 2
	            break;
	        case 3:
	            P4->OUT &= ~0xFF;   //Sets all bits/segments to Input
	            P4->OUT |= 0b01001111; //Uses segments to create a 3
	            break;
	        case 4:
	            P4->OUT &= ~0xFF;   //Sets all bits/segments to Input
	            P4->OUT |= 0b01100110; //Uses segments to create a 4
	            break;
	        case 5:
	            P4->OUT &= ~0xFF;   //Sets all bits/segments to Input
	            P4->OUT |= 0b01101101; //Uses segments to create a 5
	            break;
	        case 6:
	            P4->OUT &= ~0xFF;   //Sets all bits/segments to Input
	            P4->OUT |= 0b01111101; //Uses segments to create a 6
	            break;
	        case 7:
	            P4->OUT &= ~0xFF;   //Sets all bits/segments to Input
	            P4->OUT |= 0b00000111; //Uses segments to create a 7
	            break;
	        case 8:
	            P4->OUT &= ~0xFF;   //Sets all bits/segments to Input
	            P4->OUT |= 0b01111111; //Uses segments to create a 8
	            break;
	        case 9:
	            P4->OUT &= ~0xFF;   //Sets all bits/segments to Input
	            P4->OUT |= 0b01101111; //Uses segments to create a 9
	            break;
	        case 10:
	            Number = 0; //Sets Number equal to 0
	            break;
	        default:
	            Number = 0; //If nothing is selected, set Number to 0
	            break;
	        }
	    }
}

void PORT5_IRQHandler(){
    __delay_cycles(1000000);
    P5->IFG &= ~BIT6;
    while(!(P5->IN & BIT6));
    Number = Number+1;
}

void PORT2_IRQHandler(){
    __delay_cycles(1000000);
    P2->IFG &= ~(BIT4|BIT6);
    while(!(P2->IN & (BIT4|BIT6)));
    Number= Number-1;
}
