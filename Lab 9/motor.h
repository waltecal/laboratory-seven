/************************************
 * Names: Caleb Walters, Wayne Pitts
 *              Damon Schneider
 * Date: 10/23/2019
 * Instructor: Prof. Trevor Ekin
 * Class: EGR 226-904
 * Description: This code sets up the
 * motor
 ***********************************/
/*
 * motor.h
 *
 *  Created on: Oct 29, 2019
 *      Author: pitts_000
 */
float dutycycle = .1;

void SetupMotor(){
    P6->SEL0 |= BIT7; //giving TA2.4 control of P6.6
    P6->SEL1 &= ~BIT7;
    P6->DIR |= BIT7;

    TIMER_A2->CTL = 0b000001010010100; //SMLK, divide by 2, count up, resets timer
    TIMER_A2->CCR[0] = 37500; //3000000 / 40 / 2 for the period
    TIMER_A2->CCTL[4] = 0xE0; //set/reset mode
    TIMER_A2->CCR[4] = 37500 * dutycycle; //50% duty cycle for TA2.3 which is P6.6

}


