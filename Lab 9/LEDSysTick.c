/************************************
 * Names: Caleb Walters, Wayne Pitts
 *              Damon Schneider
 * Date: 10/23/2019
 * Instructor: Prof. Trevor Ekin
 * Class: EGR 226-904
 * Description: This code uses a
 * Systick timer to print a digit
 * onto a 7 segment LED
 ***********************************/
#include "msp.h"      //Includes necessary libraries


int Number = 0;     //Sets global variable Number to 0

void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer

    P4->SEL0 &= ~0x7F;
    P4->SEL1 &= ~0x7F;        //Sets bits  to GPIO on port 4
    P4->DIR |= 0x7F;          //Sets 4.7 as output
    P4->OUT &= ~0x7F;         //Makes 4.7 a low output

    __enable_interrupt();   //Calls enable interrupt function
    NVIC_EnableIRQ(SysTick_IRQn);   //Allows interrupts

    SysTick->CTRL =0;   //Stops Systick
    SysTick->LOAD = 3000000;  //1 sec at 3MHz  //Reload register STRVR
    SysTick->VAL = 226; //any value clears count
    SysTick->CTRL |= (BIT1|BIT0);  //Enable   //Status and Control Register STCSR and intterupts

    while(1){       //Runs
        switch (Number){    //When number is ... run below

        case 0:     //When case is 0
            P4->OUT &= ~0xFF;   //Makes all bits input
            P4->OUT |= 0b00111111; //Uses segments to create a 0
            break;
        case 1:     //When case is 1
            P4->OUT &= ~0xFF;   //Makes all bits input
            P4->OUT |= 0b00000110; //Uses segments to create a 1
            break;
        case 2:     //When case is 2
            P4->OUT &= ~0xFF;   //Makes all bits input
            P4->OUT |= 0b01011011; //Uses segments to create a 2
            break;
        case 3:     //When case is 3
            P4->OUT &= ~0xFF;   //Makes all bits input
            P4->OUT |= 0b01001111; //Uses segments to create a 3
            break;
        case 4:     //When case is 4
            P4->OUT &= ~0xFF;   //Makes all bits input
            P4->OUT |= 0b01100110; //Uses segments to create a 4
            break;
        case 5:     //When case is 5
            P4->OUT &= ~0xFF;   //Makes all bits input
            P4->OUT |= 0b01101101; //Uses segments to create a 5
            break;
        case 6:     //When case is 6
            P4->OUT &= ~0xFF;   //Makes all bits input
            P4->OUT |= 0b01111101; //Uses segments to create a 6
            break;
        case 7:     //When case is 7
            P4->OUT &= ~0xFF;   //Makes all bits input
            P4->OUT |= 0b00000111; //Uses segments to create a 7
            break;
        case 8:     //When case is 8
            P4->OUT &= ~0xFF;   //Makes all bits input
            P4->OUT |= 0b01111111; //Uses segments to create a 8
            break;
        case 9:     //When case is 9
            P4->OUT &= ~0xFF;   //Makes all bits input
            P4->OUT |= 0b01101111; //Uses segments to create a 9
            break;

        }
    }
}

void SysTick_Handler(){
    Number += 1;    //Adds 1 to the value of Number

    if(Number == 10){   //If Number is equal to 10 run below
        Number = 0;     //Sets Number back to 0
    }
}
