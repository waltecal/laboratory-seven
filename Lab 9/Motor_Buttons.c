/************************************
 * Names: Caleb Walters, Wayne Pitts
 *              Damon Schneider
 * Date: 10/23/2019
 * Instructor: Prof. Trevor Ekin
 * Class: EGR 226-904
 * Description: This code allows a
 * user to press a button to speed
 * up, slow down, or stop a motor
 ***********************************/
#include "msp.h"
#include "motor.h"      //Includes all necessary libraries
#include <stdio.h>


/**
 * main.c
 */
void main(void)
{
    WDT_A->CTL = WDT_A_CTL_PW | WDT_A_CTL_HOLD;		// stop watchdog timer

    P5->SEL0 &= ~BIT6;
    P5->SEL1 &= ~BIT6;        //Sets P5.6 as GPIO
    P5->DIR &= ~BIT6;          //Sets 5.6 as output
    P5->REN |= BIT6;          //Enables Pull-up on 5.6
    P5->OUT |= BIT6;         //Makes 5.6 a high output


    P2->SEL0 &= ~(BIT4|BIT6);
    P2->SEL1 &= ~(BIT4|BIT6);       //Sets bits 2 and 4 to GPIO on port 2
    P2->DIR &= ~(BIT4|BIT6);        //Sets P2.4 & P2.6 as input
    P2->REN |= (BIT4|BIT6);         //Enables a pull up on P2.4 & P2.6
    P2->OUT |= (BIT4|BIT6);         //Sets P2.4 & P2.6 as a high
    SetupMotor();       //Calls SetupMotor function

    while(1){       //runs
        if(!(P5->IN & BIT6)){       //Runs if the white button is pressed
            printf("White Button Pressed\n");   //Prints statement
            if(dutycycle <= 1){     //Runs below if duty cycle varaible is less than 1
                dutycycle = dutycycle + .1;     //Adds .1 to the value of dutycycle
                SetupMotor();       //Calls SetupMotor function
            }
            printf("Duty cycle is now %g\n", dutycycle);    //Prints the new dutycycle value

        }
        if(!(P2->IN & BIT4)){       //Runs if the blue button is pressed
            printf("Blue Button Pressed\n");    //Prints statement
            if(dutycycle >=.1){     //If duty cycle is more than .1
                dutycycle = dutycycle - .1;     //Subtracts .1 from the value of dutycycle
                SetupMotor();       //Calls SetupMotor function
            }
            printf("Duty cycle is now %g\n", dutycycle);      //Prints the new dutycycle value
        }
        if(!(P2->IN & BIT6)){       //Runs if the black button is pressed
            printf("Black Button Pressed\n");       //Prints statement
            dutycycle = 0;      //Sets dutycycle to 0
            SetupMotor();       //Calls SetupMotor function
            printf("Motor Off\n");      //Prints statement
        }
    }
}
